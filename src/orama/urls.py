# django
from django.contrib import admin
from django.urls import path, include

# our apps
from freelancer import urls as urls_freelancer

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(urls_freelancer) )
]
