from django.test import TestCase

from freelancer.serializers.professionalExperienceSerializer import ProfessionalExperience

class ProfessionalExperienceTests(TestCase):
    def test_consistent_dates(self):
        """
        check if ProfessinalExperience() consistent_dates method returns false if 
        end date <= than start date
        """

        experienceA = ProfessionalExperience({"id":1, "companyName": "Company A", 'startDate':"2018-05-01T00:00:00+01:00", "endDate":"2016-01-01T00:00:00+01:00", "skills":[{"id": 241, "name": "React"}]})
        experienceB = ProfessionalExperience({"id":2, "companyName": "Company B", "startDate":"2019-01-01T00:00:00+01:00", "endDate":"2019-01-01T00:00:00+01:00", "skills":[{"id": 370, "name": "Javascript"}]})

        self.assertIs(experienceA.consistent_dates(), False)
        self.assertIs(experienceB.consistent_dates(), False)
          