# rest framework
from rest_framework import serializers

# our apps
from .skillSerializer import Skill


class ProfessionalExperience(serializers.Serializer):
    id = serializers.IntegerField()
    companyName = serializers.CharField()
    startDate = serializers.DateTimeField()
    endDate = serializers.DateTimeField()
    skills = Skill(many=True)

    def consistent_dates(self):
        return self.data['endDate'] > self.data['startDate']