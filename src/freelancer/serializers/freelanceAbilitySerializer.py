# rest framework
from rest_framework import serializers

# our app
from .userSerializer import User
from .professionalExperienceSerializer import ProfessionalExperience

class Freelancer(serializers.Serializer):
    id = serializers.IntegerField()
    user = User()
    status = serializers.CharField()
    retribution = serializers.IntegerField()
    availabilityDate = serializers.DateTimeField()
    professionalExperiences = ProfessionalExperience(many=True)