# rest framework
from rest_framework import serializers

class Skill(serializers.Serializer):
    id = serializers.IntegerField()
    name = serializers.CharField()