# rest framework
from rest_framework import serializers

class User(serializers.Serializer):
    firstName = serializers.CharField()
    lastName = serializers.CharField()
    jobTitle = serializers.CharField()