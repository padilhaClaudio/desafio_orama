# rest framework
from rest_framework import views
from rest_framework.response import Response

# our apps
from freelancer.serializers.freelanceAbilitySerializer import Freelancer
from freelancer.serializers.professionalExperienceSerializer import ProfessionalExperience

# other parties
import pandas as pd 

def get_computed_skills(professionalExperiences):

    computedSkils = []

    for professionalExperience in professionalExperiences:
        if not ProfessionalExperience(professionalExperience).consistent_dates():
            raise ValueError()

        candidate_months = pd.date_range(professionalExperience['startDate'],professionalExperience['endDate'], freq='MS').strftime("%Y-%b").tolist()

        for skill in professionalExperience['skills']:
            if not any(x['id'] == skill['id'] for x in computedSkils):
                computedSkils.append({
                    'id': skill['id'],
                    'name': skill['name'],
                    'months': []
                })
            
            computed_skill = next(item for item in computedSkils if item['id'] == skill['id'])

            for month in candidate_months:
                if month not in computed_skill['months']:
                    computed_skill['months'].append(month)

    for skill in computedSkils:
        skill['durationInMonths'] = len(skill['months'])
        del skill['months']

    return sorted(computedSkils, key=lambda x: x['id'])


class GetFreelancerAbilities(views.APIView):

    def post(self, request):

        serializer = Freelancer(data=request.data['freelance'])

        if serializer.is_valid():

            try:
                return Response({
                    'freelance': {
                        'id': serializer.data['id'],
                        'computedSkills': get_computed_skills(serializer.data['professionalExperiences'])
                    }
                })      
            except ValueError:
                return Response({'error_message': 'Inconsistent end date and start date!'}, status=400)
        else:
            return Response({'error_message': 'Erro no payload!'}, status=422)