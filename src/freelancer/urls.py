# django
from django.urls import path

# our apps
from .views.freelancerAbilityAPIView import GetFreelancerAbilities

urlpatterns = [
    path('freelancerAbilities/', GetFreelancerAbilities.as_view())
]